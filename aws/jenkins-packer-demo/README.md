# Jenkins packer demo build

```shell script
ARTIFACT=`packer build -machine-readable packer-demo.json |awk -F, '$0 ~/artifact,0,id/ {print $6}'`
AMI_ID=`echo $ARTIFACT | cut -d ':' -f2`
echo 'variable "instance_ami" { default = "'${AMI_ID}'" }' > amivar.tf
aws s3 cp amivar.tf s3://terraform-state-koltsovd/amivar.tf
```

Configure jenkins by running:

```shell script
su - jenkins
aws configure
```

# Jenkins terraform build

```shell script
cd jenkins-packer-demo/app
aws s3 cp s3://terraform-state-koltsovd/amivar.tf amivar.tf
make init
make apply-ci ADD_VARS="-var 'instance_count=1'"
```
