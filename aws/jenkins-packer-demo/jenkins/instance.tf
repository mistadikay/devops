resource "aws_instance" "jenkins-instance" {
  ami           = var.amis[var.region]
  instance_type = "t3.micro"

  # the VPC subnet
  subnet_id = data.terraform_remote_state.vpc.outputs.public_subnet_id_1

  # the security group
  vpc_security_group_ids = [aws_security_group.jenkins-securitygroup.id]

  # the public SSH key
  key_name = var.key_name

  # user data
  user_data = data.template_cloudinit_config.cloudinit-jenkins.rendered
}

resource "aws_ebs_volume" "jenkins-data" {
  availability_zone = "eu-north-1a"
  size              = 20
  type              = "gp2"
  tags = {
    Name = "jenkins-data"
  }
}

resource "aws_volume_attachment" "jenkins-data-attachment" {
  device_name  = var.device_name
  volume_id    = aws_ebs_volume.jenkins-data.id
  instance_id  = aws_instance.jenkins-instance.id
  skip_destroy = true
}
