variable "name" {}
variable "region" {}
variable "remote_state_bucket" {}
variable "remote_state_path" {}

variable "key_name" {
  default = "mykey"
}

variable "amis" {
  type = map(string)
  default = {
    // Ubuntu 16.04 LTS
    eu-north-1 = "ami-017ad30b324faed9b"

    // Ubuntu 18.04 LTS
    //    eu-north-1 = "ami-0e850e0e9c20d9deb"
  }
}

variable "device_name" {
  default = "/dev/xvdh"
}

// see https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/nvme-ebs-volumes.html
variable "nitro_device_name" {
  default = "/dev/nvme1n1"
}

variable "jenkins_version" {
  default = "2.204.1"
}

variable "terraform_version" {
  default = "0.12.21"
}
