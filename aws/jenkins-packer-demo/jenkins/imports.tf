data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = var.remote_state_bucket
    key    = "terraform/${var.region}/vpc.tfstate"
    region = var.region
  }
}
