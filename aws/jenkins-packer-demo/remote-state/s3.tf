resource "aws_s3_bucket" "terraform-state" {
  bucket = "terraform-state-koltsovd"
  acl    = "private"

  tags = {
    Name = "Terraform state"
  }
}
