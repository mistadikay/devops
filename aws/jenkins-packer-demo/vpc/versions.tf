terraform {
  required_version = ">= 0.12"

  backend "s3" {
    config = {
      bucket = var.remote_state_bucket
      key    = var.remote_state_path
      region = var.region
    }
  }
}
