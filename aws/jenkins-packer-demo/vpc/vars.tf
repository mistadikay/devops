variable "name" {}
variable "region" {}
variable "remote_state_bucket" {}
variable "remote_state_path" {}
variable "key_name" {
  default = "mykey"
}
