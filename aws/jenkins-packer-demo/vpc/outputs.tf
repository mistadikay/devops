output "id" {
  value = aws_vpc.main.id
}

output "public_subnet_id_1" {
  value = aws_subnet.main-public-1.id
}
