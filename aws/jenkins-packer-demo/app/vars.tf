variable "name" {}
variable "region" {}
variable "remote_state_bucket" {}
variable "remote_state_path" {}

variable "key_name" {
  default = "mykey"
}

variable "terraform_version" {
  default = "0.12.21"
}

variable "instance_count" {
  default = "0"
}

