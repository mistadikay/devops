resource "aws_instance" "app-instance" {
  count         = var.instance_count
  ami           = var.instance_ami
  instance_type = "t3.micro"

  # the VPC subnet
  subnet_id = data.terraform_remote_state.vpc.outputs.public_subnet_id_1

  # the security group
  vpc_security_group_ids = [aws_security_group.app-securitygroup.id]

  # the public SSH key
  key_name = var.key_name
}
