AWS_REGION=eu-north-1
REMOTE_STATE_BUCKET=terraform-state-koltsovd
REMOTE_STATE_PATH=terraform/$(AWS_REGION)/$(PROJECT).tfstate
PLAN_FILE=./prepared.plan

ADD_VARS=
COMMON_VARS=-var 'region=$(AWS_REGION)' -var 'name=$(PROJECT)' -var 'remote_state_bucket=$(REMOTE_STATE_BUCKET)' -var 'remote_state_path=$(REMOTE_STATE_PATH)'

get:
	terraform get

show: get
	terraform show

destroy-plan: get
	terraform plan -destroy $(ADD_VARS) $(COMMON_VARS) -out=destroy.plan

destroy: get
	terraform destroy $(ADD_VARS) $(COMMON_VARS)

plan: get
	terraform plan $(ADD_VARS) $(COMMON_VARS)

prepare-plan: get
	terraform plan $(ADD_VARS) $(COMMON_VARS) -out=$(PLAN_FILE)

execute-plan: apply-plan

apply-plan: get
	terraform apply $(PLAN_FILE)
	rm $(PLAN_FILE)

apply: get
	terraform apply $(ADD_VARS) $(COMMON_VARS)

apply-ci: get
	terraform apply -auto-approve $(ADD_VARS) $(COMMON_VARS)

init:
	terraform init

init-remote-state:
	terraform init $(COMMON_VARS) \
		-backend-config="bucket=$(REMOTE_STATE_BUCKET)" \
		-backend-config="key=$(REMOTE_STATE_PATH)" \
		-backend-config="region=$(AWS_REGION)"